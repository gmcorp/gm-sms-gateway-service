﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.tbx_mobile = new System.Windows.Forms.TextBox();
            this.tbx_response = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(260, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Enviar Mensaje";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbx_mobile
            // 
            this.tbx_mobile.Location = new System.Drawing.Point(12, 12);
            this.tbx_mobile.Name = "tbx_mobile";
            this.tbx_mobile.Size = new System.Drawing.Size(260, 20);
            this.tbx_mobile.TabIndex = 1;
            // 
            // tbx_response
            // 
            this.tbx_response.Location = new System.Drawing.Point(12, 88);
            this.tbx_response.Multiline = true;
            this.tbx_response.Name = "tbx_response";
            this.tbx_response.ReadOnly = true;
            this.tbx_response.Size = new System.Drawing.Size(260, 161);
            this.tbx_response.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.tbx_response);
            this.Controls.Add(this.tbx_mobile);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbx_mobile;
        private System.Windows.Forms.TextBox tbx_response;
    }
}

