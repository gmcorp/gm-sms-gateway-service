﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : Form
    {
        SMSService.SmsAdapter objSMSAdapter = new SMSService.SmsAdapter();
        public Form1()
        {
            InitializeComponent();

            tbx_mobile.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mobile = "+" + tbx_mobile.Text.Trim();
            string response = objSMSAdapter.sendShortMessage("Dato", "Dato", mobile, "Claro PE", "Hola Mundo!", "7777", 1);

            tbx_response.Text = response;
        }
    }
}
