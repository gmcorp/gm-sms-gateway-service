﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace gatewaysms
{
    public class SmsAdapterSender
    {
        string publicResponse = "GMSMS : Response error message";
        public string SendSMSByKannelServer(string carrier, string mobile, string message)
        {
            try
            {
                string html = string.Empty;
                string url = GetURL(carrier, mobile, message);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }
                int x = publicResponse.Length;
                if (html.Substring(0, 1) == "0")
                    html = getUUIDResponce();
                else
                    html = publicResponse + " " + html;
                return html;
            }
            catch (Exception ex)
            {
                return publicResponse + " " + ex.Message;
            }
        }

        public string SendByBitelService(string mobile, string message)
        {
            try
            {
                string response = string.Empty;
                string User = string.Empty;
                string Password = string.Empty;
                string urlBitel = string.Empty;
                BitelSMS.SmsWSClient objBitelSMS = new BitelSMS.SmsWSClient();
                User = ConfigurationManager.AppSettings["BitelUser"];
                Password = ConfigurationManager.AppSettings["BitelPassword"];
                response = objBitelSMS.sendSMS(User, Password, GetBitelNumber(mobile), message, string.Empty);
                if (response.Substring(0, 1) == "0")
                    response = getUUIDResponce();
                else
                    response = publicResponse + " " + response;
                return response;
            }
            catch (Exception ex)
            {
                return publicResponse + " " + ex.Message;
            }
        }

        private string GetBitelNumber(string mobile)
        {
            return mobile.Substring(mobile.Length - 9);
        }

        private string GetURL(string carrier, string mobile, string message)
        {
            string url = ConfigurationManager.AppSettings["URL"];
            string User = string.Empty;
            string Password = string.Empty;
            string From = string.Empty;
            string Binfo = string.Empty;
            string Smsc = string.Empty;
            string urlBitel = string.Empty;
            switch (carrier.ToUpper().Replace(" ", string.Empty))
            {
                case "BITELPE":
                    User = ConfigurationManager.AppSettings["BitelUser"];
                    Password = ConfigurationManager.AppSettings["BitelPassword"];
                    From = ConfigurationManager.AppSettings["BitelFrom"];
                    Binfo = ConfigurationManager.AppSettings["BitelBinfo"];
                    Smsc = ConfigurationManager.AppSettings["BitelSmsc"];
                    urlBitel = ConfigurationManager.AppSettings["BitelURL"];
                    break;
                case "CLAROPE":
                    User = ConfigurationManager.AppSettings["ClaroUser"];
                    Password = ConfigurationManager.AppSettings["ClaroPassword"];
                    From = ConfigurationManager.AppSettings["ClaroFrom"];
                    Binfo = ConfigurationManager.AppSettings["ClaroBinfo"];
                    Smsc = ConfigurationManager.AppSettings["ClaroSmsc"];
                    break;
                case "ENTELPE":
                    User = ConfigurationManager.AppSettings["EntelUser"];
                    Password = ConfigurationManager.AppSettings["EntelPassword"];
                    From = ConfigurationManager.AppSettings["EntelFrom"];
                    Binfo = ConfigurationManager.AppSettings["EntelBinfo"];
                    Smsc = ConfigurationManager.AppSettings["EntelSmsc"];
                    break;
                case "MOVISTARPE":
                    User = ConfigurationManager.AppSettings["MovistarUser"];
                    Password = ConfigurationManager.AppSettings["MovistarPassword"];
                    From = ConfigurationManager.AppSettings["MovistarFrom"];
                    Binfo = ConfigurationManager.AppSettings["MovistarBinfo"];
                    Smsc = ConfigurationManager.AppSettings["MovistarSmsc"];
                    break;
                default:
                    break;
            }
            message = UrlEncode(message);
            url = String.Format(url, User, Password, mobile, From, message, Binfo, Smsc);
            return url;
        }

        public string SendNotification(string deviceId, string message)
        {
            string SERVER_API_KEY = ConfigurationManager.AppSettings["SERVER_API_KEY"];
            var SENDER_ID = ConfigurationManager.AppSettings["SENDER_ID"];
            var value = message;
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&registration_id=" + deviceId + "";
            Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;
        }


        

        public string UrlEncode(string value)
        {
            string reservedCharacters = "!*'();:@&=+$,/?%#[]";
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (reservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);
                else
                    sb.AppendFormat("%{0:X2}", (int)@char);
            }
            return sb.ToString();
        }

        private string getUUIDResponce()
        {
            string strUUID = Guid.NewGuid().ToString("N");
            return strUUID.Substring(0, 16);
        }
    }
}