﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace gatewaysms
{
    /// <summary>
    /// Summary description for gatewaysms
    /// </summary>
    [WebService(Namespace = "http://olympus.pe/webservices/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SmsAdapter : System.Web.Services.WebService
    {

        /// <remarks/>
        [WebMethod]
        public string sendMessage(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, string ServiceType, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
        }

        /// <remarks/>
        [WebMethod]
        public string sendMessageWithTransactionID(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, string ServiceType, string TransactionID, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
        }

        /// <remarks/>
        [WebMethod]
        public string sendShortMessage(string Serial, string Pin, string Mobile, string Carrier, string Message, string Sender, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
            {
                // RZevallos - 12/09/2018 - Integración Infobip
                Log.LogDebug("Ingresa SendShortMessage");

                bool infobipIsEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["INFOBIP_IS_ENABLED"]);

                Log.LogDebug("InfobipIsEnabled: " + infobipIsEnabled);

                if (infobipIsEnabled)
                {
                    InfobipConnection infobipConnection = new InfobipConnection();

                    return infobipConnection.SendMessage(Mobile, Message);
                }

                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
            }
        }

        /// <remarks/>
        [WebMethod]
        public string sendShortMessageWithTransactionID(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, int CampaignID, string TransactionID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
        }

        /// <remarks/>
        [WebMethod]
        public string sendPremiumMessage(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
            {
                // RZevallos - 13/09/2018 - Integración Infobip
                Log.LogDebug("Ingresa SendPremiumMessage");

                bool infobipIsEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["INFOBIP_IS_ENABLED"]);

                Log.LogDebug("InfobipIsEnabled: " + infobipIsEnabled);

                if (infobipIsEnabled)
                {
                    InfobipConnection infobipConnection = new InfobipConnection();

                    return infobipConnection.SendMessage(Mobile, Message);
                }

                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
            }
        }

        /// <remarks/>
        [WebMethod]
        public string sendFreeMessage(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
            {
                Log.LogDebug("Ingresa SendFreeMessage");

                bool infobipIsEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["INFOBIP_IS_ENABLED"]);

                Log.LogDebug("InfobipIsEnabled: " + infobipIsEnabled);

                if (infobipIsEnabled)
                {
                    InfobipConnection infobipConnection = new InfobipConnection();

                    return infobipConnection.SendMessage(Mobile, Message);
                }

                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
            }   
        }

        /// <remarks/>
        [WebMethod]
        public string sendConfirmationMessage(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
            {
                // RZevallos - 13/09/2018 - Integración Infobip
                Log.LogDebug("Ingresa SendConfirmationMessage");

                bool infobipIsEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["INFOBIP_IS_ENABLED"]);

                Log.LogDebug("InfobipIsEnabled: " + infobipIsEnabled);

                if (infobipIsEnabled)
                {
                    InfobipConnection infobipConnection = new InfobipConnection();

                    return infobipConnection.SendMessage(Mobile, Message);
                }

                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
            }
        }

        /// <remarks/>
        [WebMethod]
        public string sendPremiumMessageByServiceType(string Serial, string Pin, string Carrier, string Mobile, string Message, string Sender, string ServiceType, int CampaignID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
        }

        /// <remarks/>
        [WebMethod]
        public string sendBinaryMessage(string Serial, string Pin, string Mobile, string Carrier, string Message, string Sender, int CampaignID, int LibraryID)
        {
            SmsAdapterSender objSendSMS = new SmsAdapterSender();
            if (Carrier.ToUpper().Replace(" ", string.Empty) == "BITELPE")
                return objSendSMS.SendByBitelService(Mobile, Message);
            else
                return objSendSMS.SendSMSByKannelServer(Carrier, Mobile, Message);
        }
    }
}
