﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gatewaysms
{
    public class Log
    {
        public static LctrSysLog.Log log = new LctrSysLog.Log("GMONEY_GATEWAY_SMS");

        public static void LogDebug(string message)
        {
            log.WriteLog(LctrSysLog.Log.facilidad.LogAlerta, LctrSysLog.Log.severidad.Depurador, message);
        }
        public static void LogException(Exception exception)
        {
            log.WriteLog(LctrSysLog.Log.facilidad.LogAlerta, LctrSysLog.Log.severidad.Depurador, "Exception: " + exception.ToString());
        }
    }
}