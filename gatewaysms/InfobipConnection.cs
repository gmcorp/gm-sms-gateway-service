﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace gatewaysms
{
    public class InfobipConnection
    {
        private static string SERVICE = ConfigurationManager.AppSettings["INFOBIP_SERVICE"];
        private static string USERNAME = ConfigurationManager.AppSettings["INFOBIP_USERNAME"];
        private static string PASSWORD = ConfigurationManager.AppSettings["INFOBIP_PASSWORD"];
        private string token = string.Empty;

        private static int PENDING_STATUS = 1;
        private static int DELIVERED_STATUS = 1;

        public InfobipConnection()
        {
            try
            {
                var _token = System.Text.Encoding.UTF8.GetBytes(USERNAME + ":" + PASSWORD);

                token = "Basic " + System.Convert.ToBase64String(_token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SendMessage(string mobile, string message)
        {
            var client = new RestClient(SERVICE);

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", token);
            request.AddParameter("application/json", GenerateRequest(mobile, message), ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            InfobipResponse infobipResponse = JsonConvert.DeserializeObject<InfobipResponse>(response.Content);

            Log.LogDebug("InfobipResponse: " + response.Content);

            if (infobipResponse.data.Count > 0)
            {
                InfobipData infobipData = infobipResponse.data[0];

                if (infobipData.Status.GroupId == PENDING_STATUS || infobipData.Status.GroupId == DELIVERED_STATUS)
                {
                    return GetUuidResponse();
                }
                else
                {
                    return infobipData.Status.Description;
                }
            }

            return "GMSMS : Response error message";
        }

        private string GenerateRequest(string mobile, string message)
        {
            string json = "{\"from\":\"InfoSMS\", \"to\":\"{{MOBILE}}\",\"text\":\"{{MESSAGE}}\"}";

            return json.Replace("{{MOBILE}}", mobile).Replace("{{MESSAGE}}", message);
        }

        private string GetUuidResponse()
        {
            string uuid = Guid.NewGuid().ToString("N");

            return uuid.Substring(0, 16);
        }
    }

    public class InfobipResponse
    {
        [JsonProperty(PropertyName = "messages")]
        public List<InfobipData> data { get; set; }
    }

    public class InfobipData
    {
        [JsonProperty(PropertyName = "to")]
        public string Mobile { get; set; }

        [JsonProperty(PropertyName = "messageId")]
        public string MessageId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public Status Status { get; set; }
    }

    public class Status
    {
        [JsonProperty(PropertyName = "groupId")]
        public int GroupId { get; set; }                // Status/Error group ID.

        [JsonProperty(PropertyName = "groupName")]
        public string GroupName { get; set; }           // Status/Error group name.

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }                     // Status/Error ID.

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }                // Status/Error name.

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }         // Human-readable description of the status/error.
    }
}